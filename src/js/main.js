'use strict'

// import * as $ from 'jquery'
import 'materialize-css/dist/js/materialize.min.js'

let scrolled = 0, fixed

document.addEventListener('DOMContentLoaded', function() {

    let menuToggle      = document.querySelector('.menu-toggle'),
        backendToggle   = document.querySelector('.menu-toggle-backend'),
        overlay         = document.querySelector('.overlay'),
        heroSection     = document.querySelector('.section-hero'),
        logo            = document.querySelector('header nav a.logo'),
        nav             = document.querySelector('.header-fixture'),
        countInputs     = document.querySelectorAll('input.count')

    let navOffset       = nav.getBoundingClientRect().top,
        hideOffset      = heroSection.offsetHeight / 2

    M.AutoInit()
    if (countInputs) M.CharacterCounter.init(countInputs)

    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.parallax')
        var instances = M.Parallax.init(elems, options)
    })

    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.sidenav')
        var instances = M.Sidenav.init(elems, options)
    })

    if (heroSection.dataset.logo !== undefined) {
        if (scrolled <= hideOffset) {
            logo.classList.add('hidden')
        }

        document.addEventListener('scroll', e => {
            if (scrolled > hideOffset && logo.classList.contains('hidden')) {
                logo.classList.remove('hidden')
            } else if (scrolled <= hideOffset && !logo.classList.contains('hidden')) {
                logo.classList.add('hidden')
            }
        })
    }

    document.addEventListener('scroll', e => {
        scrolled = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop

        if (scrolled > navOffset && !nav.classList.contains('fixed')) {
            nav.classList.add('fixed')
        } else if (scrolled <= navOffset && nav.classList.contains('fixed')) {
            nav.classList.remove('fixed')
        }
    })
}, false)
